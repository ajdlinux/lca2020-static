---
layout: page
title: Miniconfs
permalink: /programme/miniconfs/
sponsors: true
---

The first two days of linux.conf.au are made up of dedicated day-long streams focussing on single topics.
They are organised and run by volunteers and are a great way to kick the week off.

First introduced at linux.conf.au 2002, they are now a traditional element of the conference.
They were originally intended as an incubator -- both of future conferences and speakers.

Although delegates who present at miniconfs are not afforded speaker privileges at the main conference, speaking at a miniconf is a great way to gain experience, provide exposure for your project or topic, and raise your professional profile.
