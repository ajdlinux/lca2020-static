## linux.conf.au 2020 website

https://lca2020.linux.org.au

## Development

This site uses [jekyll](https://jekyllrb.com/), a Ruby-based static website generator

## Local environment

``` shell
gem install jekyll
jekyll serve -w
```
