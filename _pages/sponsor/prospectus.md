---
layout: page
title: Sponsorship Prospectus
permalink: /sponsors/prospectus/
sponsors: false
---

## How to sponsor

Contact [sponsorship@lca2020.linux.org.au](mailto:sponsorship@lca2020.linux.org.au) for more information and a copy of our prospectus.

## Why Sponsor

### Overview

One of the world’s best free and open source technology conferences, linux.conf.au is regularly attended by over 600 delegates, more than half of whom attend as Professional Delegates. Delegates travel from all over the world for a week of thought provoking keynotes from our internationally recognised invited speakers, high quality presentations and tutorials from the makers of these technologies, and connecting with other delegates in both organised networking sessions and spontaneous encounters in the hallways.

Sponsors of linux.conf.au have a unique opportunity to globally promote their brand to a large number of influential industry professionals. To discuss the sponsorship packages and opportunities, please contact our sponsor liaison at [sponsorship@lca2020.linux.org.au](mailto:sponsorship@lca2020.linux.org.au).

### Sponsor Early

Agreeing to sponsor linux.conf.au 2020 early is important.
The earlier you sign a sponsorship agreement, and we have a purchase order completed, the more time we can spend promoting your sponsorship.

### Diversity and Inclusion

linux.conf.au aims to be an inclusive event which welcomes diverse groups from all parts of the FLOSS (Free, Libre, Open Source Software) community to an environment of respect, tolerance, and encouragement.
linux.conf.au has an enforced [Code of Conduct](/attend/code-of-conduct/), with all complaints treated confidentially and seriously.

## Sponsorship Packages

All sponsors are acknowledged each day of the conference during the opening session.

### Emperor Penguin
Emperor Penguin Sponsor is our highest level of sponsorship available. It provides the greatest opportunity for brand exposure and participation in key conference events.

Sponsorship package includes:
* Identification as an Emperor Penguin sponsor of the conference
* Five Professional Tickets
* Four additional tickets at the early bird price
* Three additional PDNS-only tickets to permit additional representation at the PDNS
* An opportunity to address the professional delegates during the PDNS
* Promotion via banners at the keynote venue, at the conference dinner, at the PDNS, and around the conference venue
* Promotion of your logo prominently on the conference website and every
delegate badge

### King Penguin
The King Penguin Sponsorship package is available for organisations who wish to make a significant contribution to the conference experience

Sponsorship package includes:
* Identification as a King Penguin sponsor of the conference
* Four Professional Tickets
* Three additional tickets at the early bird price
* Two additional PDNS-only tickets to permit additional representation at the PDNS
* Promotion via banners at the PDNS and around the conference venue
* Promotion of your logo on the conference website sponsors page

### Royal Penguin
Sponsorship package includes:
* Identification as a Royal Penguin sponsor of the conference
* Three Professional Tickets
* Up to two additional tickets at the early bird price
* Two additional PDNS-only tickets to permit additional representation at the PDNS
* Promotion via banners around the conference venue
* Promotion of your logo on the conference website sponsors page

### Adelie Penguin
Sponsorship package includes:
* Identified as an Adelie Penguin sponsor of the conference
* One Professional Ticket
* One additional ticket at the early bird price
* One additional PDNS-only ticket to permit additional representation at the PDNS
* Promotion via a banner at the conference venue
* Promotion of your logo on the conference website sponsors page

### Fairy Penguin
Sponsorship package includes:
* Identified as a Fairy Penguin sponsor of the conference
* One Professional Ticket
* Promotion of your logo on the conference website sponsors page
* Entry level financial sponsor for companies who wish to provide support to the conference

## Additional Sponsorship Opportunities

### Lanyard Sponsor
A highly sought-after opportunity!
All delegates are required to wear identity passes attached to lanyards during linux.conf.au and its social events.
Many delegates collect lanyards and reuse them time and time again.
Your branding, along with linux.conf.au branding, will be on the lanyard, providing a high level of visibility for the associated sponsor.

### Speakers' Travel Sponsor
The sponsor will be introduced at the conference as having funded speaker’s travel grants.
Additionally, speakers who receive such grants will be informed as part of the travel booking process that their travel has been sponsored by your organisation.

### Parents' Room Sponsor
linux.conf.au seeks to encourage diversity amongst our delegates by offering a parents’ room and free childcare for delegates.
Sponsoring this would give your firm signage rights at the entrance to these facilities, a special mention during each daily opening and a logo on relevant delegate paperwork.

### Outreach and Inclusion Sponsor
Contribute directly to increasing the diversity of the free and open source technology community by sponsoring our Outreach and Inclusion Programme.
This programme reaches out to delegates from non-traditional backgrounds who would otherwise be unable to attend linux.conf.au but who have a significant contribution to make.

### Coffee Sponsor
No conference can be successful without a constant supply of high-quality coffee.
By taking up this sponsorship, you’ll enable linux.conf.au to arrange a local supplier to pull shots all week for our delegates.
Your brand will be front of mind with each caffeine hit.

### Morning/Afternoon Tea Sponsor
linux.conf.au provides morning and afternoon tea on each day of the conference.
This package allows you to have your logos displayed on each of the catering tables.
Your organisation will be mentioned as the catering sponsor for that day at morning plenary and at the assigned break.
Options are available to sponsor one or multiple days.
