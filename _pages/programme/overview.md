---
layout: page
title: Programme Overview
permalink: /programme/
sponsors: true
---

The conference starts out with two days of miniconfs followed by three days of sessions on our main program.

## Week Overview

<div class="table-responsive">
<table class="table table-striped table-bordered" summary="This table provides a summary of the conference days">
<tr>
  <th> Day </th>
  <th> Start of Day </th>
  <th>Daytime</th>
  <th>Evening</th>
</tr>
<tr>
  <th> Sunday </th>
  <td> Volunteer induction/orientation </td>
  <td> Check-in opens + First timers welcome </td>
  <td> </td>
</tr>
<tr>
  <th> Monday </th>
  <td> Conference Welcome </td>
  <td> Miniconfs </td>
  <td> Linux Australia AGM </td>
</tr>
<tr>
  <th> Tuesday </th>
  <td> Keynote #1 </td>
  <td> Miniconfs </td>
  <td> Speaker's Dinner </td>
</tr>
<tr>
  <th> Wednesday </th>
  <td> Keynote #2 </td>
  <td> Main Conference </td>
  <td> Penguin Dinner </td>
</tr>
<tr>
  <th> Thursday </th>
  <td> Keynote #3 </td>
  <td> Main Conference </td>
  <td> Professional Delegates Networking Session </td>
</tr>
<tr>
  <th> Friday </th>
  <td> Keynote #4 </td>
  <td> Main Conference <br> Lightning talks and Conference Close </td>
  <td> </td>
</tr>
</table>
</div>

## Daily Schedule Overview

<div class="table-responsive">
<table class="table table-striped table-bordered" summary="This table provides a summary of the conference days">
<tr>
  <th> Time </th>
  <th> Activity </th>
</tr>
<tr>
  <td> 9.00am </td>
  <td> Start of day and housekeeping </td>
</tr>
<tr>
  <td> 9.10am </td>
  <td> Welcoming remarks and/or keynote </td>
</tr>
<tr>
  <td> 10.10am </td>
  <td> Morning tea (catered) </td>
</tr>
<tr>
  <td> 10.45am </td>
  <td> Talks </td>
</tr>
<tr>
  <td> 12.25pm </td>
  <td> Lunch (un-catered) </td>
</tr>
<tr>
  <td> 13.40pm </td>
  <td> Talks </td>
</tr>
<tr>
  <td> 15.20pm </td>
  <td> Afternoon tea (catered) </td>
</tr>
<tr>
  <td> 15.45pm </td>
  <td> Talks </td>
</tr>
<tr>
  <td> 17.25pm </td>
  <td> Sessions end </td>
</tr>
<tr>
  <td> 17.30pm </td>
  <td> BoF sessions and evening events </td>
</tr>
</table>
</div>

## Presentation Types

### Welcome Address
The conference organisers start the week by welcoming everyone and letting everyone know the general housekeeping requirements of the conference.

### Keynotes
From Tuesday through to Friday the day starts off with a Keynote presentation.
We are really excited by our invited guests this year and will be making announcements very soon.

### Talks
Talks are 35-45 minute presentations on a single topic.
These are generally presented in lecture format and form the bulk of the available conference slots.

### Tutorials
Tutorials are 90 minute interactive learning sessions.
Particpants will gain some hands on knowledge or experience in a topic.
These are generally presented in a classroom format and are interactive or hands-on in nature.
Some tutorials will require prepartation work to be done or some hardware to be purchased prior to their attendance - please check the individual tutorial descriptions for more information.

### Lightning Talks
On Friday afternoon the traditional lightning talks (3-5 minutes each) take place.

### Conference Close
The final part of the conference will include a thanks to all volunteers, the charity raffle draw, announcement of the next year's LCA location and formal closing.
