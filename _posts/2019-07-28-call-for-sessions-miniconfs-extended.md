---
layout: post
title: Call for Sessions and Miniconfs extended
card: call_session_miniconf_date.068f02d9.png
---

We have heard that some of you would like a bit more time to submit your proposals for linux.conf.au 2020.
So, we have decided to extend the due date by two weeks to help everyone have a chance to submit. Hooray!

The call for sessions and miniconfs will now close on **Sunday 11 August 2019** [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth).
But this is the last possible date, so there will be no more extensions after this.

## How to Submit

1. Log in to your [dashboard](/dashboard/)
2. Create your speaker profile
3. Submit your proposal

If you want more information, see our [proposals page](/programme/proposals/)

## Frequently Asked Questions

### Q: What format can my talk take?

**A:** There are two main formats for sessions: presentations, which are presented in lecture format and form the bulk of conference slots; and tutorials, which are presented in a classroom format and deliver a specific learning outcome to attendees.
You can also submit a miniconference proposal, which is a full day session on a particular topic.
Miniconf organisers are responsible for finding speakers for their miniconf and organising the running of their day.

### Q: What should I include in my proposal?

**A:** You will need to write a public and a private abstract to be reviewed by the sessions selection committee.
The public abstract will be displayed on our website, while the private abstract will contain additional information about your talk to help our reviewers.
When you’re writing your proposal, think about what you want to share and what makes it interesting.
You don't need to have written your talk in order to put a proposal in, but you should have a good idea of what you want to talk about.

### Q: What can I talk about?

**A:** Our theme for linux.conf.au 2020 is “Who’s Watching”, focusing on security, privacy and ethics.
We’ve set our sights on how open source could play a role in maximising security and protecting our privacy in times of uncertainty.

Please let this inspire you, but not restrict you - we also want talks about interesting things in open technology.
Here are some ideas to get you started:
* Open source software
* Open hardware
* Open data
* Open government
* Documentation
* Community
* Security
* Privacy
* Ethics
* Open health

If you have any other questions, just drop us an [email](mailto:contact@lca2020.linux.org.au).

Don’t be afraid to throw some ideas around on Twitter using the hashtag *#linuxconfau* or *#lca2020*. We are certain the LCA community would be happy to help you workshop and refine your ideas.
