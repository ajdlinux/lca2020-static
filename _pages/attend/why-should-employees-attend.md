---
layout: page
title: Why should your employees attend linux.conf.au?
permalink: /attend/why-should-employees-attend/
sponsors: true
---

It can be tough for employers to know which conference to send their employees to.
If you're looking for a conference to help develop your team, linux.conf.au offers a holistic experience for almost anybody working in technology who has an interest in Free and Open Source Software.

linux.conf.au is a globally recognised conference, providing delegates and organisations with a strong return on investment.
The conference provides delegates with a week-long immmersive experience into everything open source.
This means delegate can bring the most up-to-date industry knowledge back to the office to share with their colleagues.

## Key information

**Dates:** Monday 13 - Friday 17 January 2020

**Venue:** Gold Coast Convention and Exhibition Centre, Gold Coast, Australia

**Tickets:** Details on pricing can be found at [https://linux.conf.au/attend/tickets/](https://linux.conf.au/attend/tickets/)

## A bit more about linux.conf.au 2020

Our theme for linux.conf.au 2020 is 'Who's Watching', focusing on security, privacy and ethics.
As big data and IoT-connected devices become more pervasive, it's no surprise that we're more concerned about privacy and security than ever before.
We've set our sights on how Free and Open Source Software could play a role in maximising security and protecting our privacy in times of uncertainty.
With the concept of privacy continuing to blur, Free and Open Source Software could be the solution to give us '2020 vision'.

While we have a lots of interesting talks on this theme, it is by no means limited to this.
You'll find talks and tutorials in software development, Linux kernel internals, DevOps, community management and more.

## What will delegates experience?

linux.conf.au is one of the world's longest-established conferences about Linux and Free and Open Source Software.

Delegates will experience:
* A selection of over 80 talks from diverse presenters who are thought leaders in the Free and Open Source Software (FOSS) community
* Access to eight hands-on tutorial sessions facilitated by highly experienced trainers
* A selection of 12 special interest tracks (Miniconfs), which give attendees an opportunity to explore a topic in Free and Open Source Software in depth and build connections with others working in the area
* A full week of professional networking with likeminded professionals

Talks, tutorials and miniconfs at the conference are highly diverse, giving delegates access to the latest groundbreaking insight in the open source industry.
The nature of linux.conf.au sees talks extend from the highly technical to business-focused case studies, which enable delegates to experience a breadth of knowledge across the technology industry.
Topics at linux.conf.au 2020 include security and privacy, architecture, open hardware and software, community engagement, diversity promotion, legal and ethical management.

Further details of our sessions are available online at [https://linux.conf.au/programme/sessions/](https://linux.conf.au/programme/sessions/).

Full details of miniconfs can be found at [https://linux.conf.au/programme/miniconfs/](https://linux.conf.au/programme/miniconfs/).

## What does your organisation gain?

Organisations who fund delegates can gain competitive advantage through:

* Access to the latest insight in the open source space
* Greater awareness and understanding of both mature and emergent technologies, enhancing delegates' strategic impact when they return to the office
* Greater awareness and understanding of issues affecting organisations and the Linux and Free and Open Source Software community, such as enhancing diversity, patent and copyright legislation and communicating to different audiences
* Enhanced technical competence and capability
* Building connections in the open source community, which can be shared around the office
* Sending someone to linux.conf.au is also a great way to recognise and reward achievement against organisational objectives

Sending your employees to linux.conf.au allows your organisation to engage with the most up-to-date industry knowledge across everything open source.

If you would like any further information, feel free to reach out by emailing [contact@lca2020.linux.org.au](mailto:contact@lca2020.linux.org.au).
