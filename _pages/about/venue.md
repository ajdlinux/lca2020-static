---
layout: page
title: Location
permalink: /about/location/
sponsors: true
---

## The City

Situated on the coast of South-East Queensland, The Gold Coast is Australia’s largest non-capital city.
With more than 300 days of a sun a year, this thriving beachside city definitely delivers the sun, sand and surf you’d expect.
If sand is not your thing (let’s face it, we’re not all avid beachgoers), the Gold Coast Hinterland is a short drive away.
Here you can explore the beautiful bushland or visit one of the wonderful wineries on offer.

Beyond the beaches, the Gold Coast is home to an iconic nightlife district, with pubs, clubs and bars to enjoy a drink.
For those looking for a little more excitement during the day we recommend visiting one of the theme parks, such as: Dreamworld, Movie World, SeaWorld or Wet’n’Wild.
As one of Australia’s most iconic destinations, the Gold Coast provides the perfect place for linux.conf.au 2020.
There are plenty of places to explore or just sit back, relax and enjoy a well-deserved break after the conference.

## The Venue

The [Gold Coast Convention and Exhibition Centre](https://www.gccec.com.au/) (GCCEC) is located at Broadbeach.
The spacious venue is near a large number of attractive accommodation options, making it easy to get to and from the conference.
Your accommodation and the venue are just a short walk from Pacific Fair Shopping Centre, local restaurants and bars, and the beautiful beaches the Gold Coast is famous for.

The venue offers contemporary facilitates, up-to-date technology and spacious rooms for talks from our industry leading speakers.
There is also plenty of space to catch up with colleagues and friends over a coffee meaning you can make the most of linux.conf.au 2020.

<div class="map-responsive">
    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=153.41875076293945%2C-28.036607022452355%2C153.4383845329285%2C-28.01959795365469&amp;layer=mapnik&amp;marker=-28.028102824051402%2C153.42856764793396" style="border: 1px solid black"></iframe><br /><small><a href="https://www.openstreetmap.org/?mlat=-28.0281&amp;mlon=153.4286#map=16/-28.0281/153.4286">View Larger Map</a></small>
</div>
