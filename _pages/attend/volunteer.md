---
layout: page
title: Volunteer
permalink: /attend/volunteer/
sponsors: true
---

linux.conf.au is a grass-roots, community-driven event.
It needs bright, enthusiastic people - like you - to make it a success!

Volunteering at linux.conf.au is a great opportunity and can be deeply rewarding.
On a more practical note, volunteering at linux.conf.au can add to your professional portfolio in many ways, making this an ideal opportunity for students or recent graduates.

By volunteering at linux.conf.au, you will have the benefit of:

* Meeting exceptional people - many of whom are recognised experts in their field and industry luminaries
* Great networking opportunities with people working in the free and open source community
* Gaining practical, hands-on experience and skills in event management, customer liaison, public relations and operation of audio visual equipment - which are invaluable to future employers

## What's required of me?
To be eligible for Volunteer Registration at linux.conf.au you need to:

* be willing to commit to at least five full days of volunteering
* able to attend the mandatory training and induction on the Sunday before the conference

The following is a indication of the tasks available across the conference week:

<div class="table-responsive">
<table class="table table-striped table-bordered" summary="This table provides a summary of the days volunteers will be needed">
  <tr>
    <th>Day</th>
    <th>Tasks available</th>
  </tr>
  <tr>
    <td>Sunday 12 Jan</td>
    <td>Conference setup and registration, AV setup, Network setup, AV training</td>
  </tr>
  <tr>
    <td>Monday 13 Jan</td>
    <td>Miniconference room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Tuesday 14 Jan </td>
    <td>Miniconference room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Wednesday 15 Jan</td>
    <td>Conference room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Thursday 16 Jan</td>
    <td>Conference room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Friday 17 Jan</td>
    <td>Conference room monitoring, AV, general assistance, and conference packup and tear-down</td>
  </tr>
  <tr>
    <td>Saturday 18 Jan</td>
    <td>Conference packup and tear-down</td>
  </tr>
</table>
</div>

**We would also like you to have one or more of the following skills and/or aptitudes:**

* the ability to meet and greet people and provide basic face to face assistance with a friendly, cheerful disposition
* some familiarity with audio visual equipment
* confidence addressing an audience, such as introducing a speaker
* some familiarity with technology such as IRC, social media, text editors etc
* a clean driver's licence, and be competent in driving minivans and small trucks
* physical fitness for some manual activities (the most you would have to do would be to lift some heavy boxes)

**The types of tasks you can be expected to be assigned to include:**

* General conference set up including moving and commissioning of equipment such as audiovisual and networking gear
* Operating audiovisual equipment such as cameras in the conference venues
* Assisting with registration of delegates and ensuring questions related to accommodation and getting around the conference venue are answered
* Introducing speakers and ensuring they run to time
* Room monitoring activities including co-ordinating with other conference rooms
* Escorting delegates to/from the Airport
* General event administration tasks

## What's in it for me?

Tempted, but want to know more about what you get for volunteering your time?
This is what we're offering:

* Free conference registration at Hobbyist level (excluding the Penguin Dinner)
* Lunch, morning tea and afternoon tea provided every day (at minimum - we may include other meals for instance if you're volunteering of an evening)
* T-shirt
* Schwag bag
* Refunds of expenses you incur in the course of volunteering (e.g. petrol for driving to the airport for a pickup)
* Written reference covering your experience and duties - useful for job interviews!

We don't include the following:

* Accommodation
* Travel to and from the conference venue - you will have to make your own way there

## Sign me up!

Volunteer registration will open soon. Please check back later for details on how to register.

Any questions? Email us at [volunteers@lca2020.linux.org.au](mailto:volunteers@lca2020.linux.org.au)
